#include <windows.h>
#include <iostream>
using namespace std;
/*
* simple repeating bleep
* Author: Benjamin Kidd
* Date: 19/12/15
*/
int main() {

	int a=0;
	short b = 0;
	cout << "Please enter a time interval in seconds:";
	cin >> a;
	cout << "\nA Beep wil now sound every " << a << " seconds.";
	while(true) {
		b++;
		cout << "\nbeeps since start:" << b;
		//frequency Hz, duration
		Beep(750, 300);
		Sleep(a*1000);
		system("cls");
	}
	return 0;
}